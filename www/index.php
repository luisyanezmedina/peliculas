<?php
session_start();
require 'app/config/config.php';
require 'app/core/autoload.php';
?>


    <?php
$router = new Router();


$controller = $router->getController();
$method = $router->getMethod();
$param = $router->getParam();


// Importa controlador por URL

if (file_exists(CONTROLLERS_URL . "{$controller}Controller.php")) {
    require CONTROLLERS_URL . "{$controller}Controller.php";
} else {
    throw new ErrorException("No se cargo el fichero: " . CONTROLLERS_URL . "{$controller}Controller.php" );
}


$controller .= 'Controller';
$controller = new $controller() ;

//Ejecutar metodo de la URL
$method .= 'Action';
$controller->$method($param);
?>

