<?php
/**
 * Class View
 * Renderea las vistas de los controladores
 */

class View
{
    protected $controllerName;
    protected $parameters;
    protected $template;
    public function __construct($controllerName, $parameters)
    {
        $this->controllerName = $controllerName;
        $this->parameters = $parameters;
        $this->renderView();
    }

    /**
     * muestra la pagina
     * @throws Exception
     */
    protected function renderView(){

        if(class_exists($this->controllerName)){
            $fileName = str_replace('Controller', '', $this->controllerName);
            $this->template = $this->getTemplate($fileName);
            echo $this->template;
        }else{
            throw new Exception("Error No existe ");
        }
    }

    /**
     * @param $fileName
     * @return string con la pagina html
     * @throws Exception
     */
    protected function getTemplate($fileName) {
        $filePath = VIEWS_URL . "$fileName" . '.php';
        if( is_file($filePath)){
            // require $filePath;
            extract($this->parameters);
            //$pagina = file_get_contents($filePath);
            ob_start();
            require($filePath);
            $pagina = ob_get_contents();
            ob_end_clean();
            return $pagina;
        }else{
            throw new Exception("Error No existe plantilla: $filePath ");
        }
    }
}
