<?php
/**
 * Class Model para conexion a DB
 */

class Model
{
    protected $db;

    public function __construct()
    {
        //$this->db = new Mysqli('docker-mysql', 'root', '1234', 'peliculas');
        $this->db = new mysqli(DH_HOST, DB_USER, DB_PASS, DB_NAME);
    }

}
