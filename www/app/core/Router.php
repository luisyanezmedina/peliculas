<?php

/**
 * Identificacion de la URI
 */
class Router
{
    /**
     * @var string
     */
    public $uri;

    /**
     * @var string
     * Nombre del controlador pasado en URL
     */
    public $controller;

    /**
     * @var string
     * Metodo HTTP usado en peticion
     */
    public $method;

    /**
     * @var string
     * Id del row a afectar
     */
    public $param;

    /**
     * ATTR inicializa
     */
    public function __construct()
    {
        $this->setUri();
        $this->setController();
        $this->setMethod();
        $this->setParam();
    }

    public function setUri()
    {
        $this->uri = explode('/', URI);
    }


    public function setController()
    {
        $this->controller = $this->uri[1] === '' ? 'Home' : $this->uri[1];
    }


    public function setMethod()
    {
        $this->method = URIMETHOD;
    }


    public function setParam()
    {
        if(URIMETHOD === 'POST'){
            $this->param = $_POST;
        }
        else if (URIMETHOD === 'GET') {
            $this->param = !empty($this->uri[2]) ? $this->uri[2] : '';
        }
        else if (URIMETHOD === 'PUT'){

            parse_str(file_get_contents('php://input'), $_PUT);
            $this->param = $_PUT;
        }
        else if (URIMETHOD === 'DELETE'){

            $this->param = $this->param = !empty($this->uri[2]) ? $this->uri[2] : '';;
        }

    }


    public function getUri()
    {
        return $this->uri;
    }


    public function getController()
    {
        return $this->controller;
    }

    public function getMethod()
    {
        return $this->method;
    }


    public function getParam()
    {
        return $this->param;
    }
}
