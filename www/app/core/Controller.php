<?php

/**
 * Class Controller
 * Logica y estructura para controladores
 */
abstract class Controller {

    private $view;

    public function __construct() {
        //echo __CLASS__. ' instanciada: ';
    }
    protected function renderView($controllerName = '', $parameters = array()) {
        $this->view = new View($controllerName, $parameters);
    }
    abstract function POSTAction($params);
    abstract function GETAction($id);


}
