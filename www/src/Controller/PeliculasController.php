<?php

require_once MODEL_URL.'PeliculasModel.php';

class PeliculasController extends Controller
{

    private $peliculasModel;
    public function __construct()
    {
        $this->peliculasModel = new PeliculasModel();
    }

    function POSTAction($params)
    {
        // TODO: Implement POSTAction() method.
        $query = $this->peliculasModel->createPelicula($params);
        if ($query){
            $response = array(
                'codigo' => 201,
                'valor' => 'Se actualizó con exito'
            );
            echo json_encode($response);
        }else{
            $response = array(
                'codigo' => 204,
                'valor' => 'Hubo un error al actualizar'
            );
            echo json_encode($response);
        }
    }

    function GETAction($id)
    {
        // TODO: Implement GETAction() method.
        $pelis = $this->peliculasModel->getPeliculaById($id);
        echo json_encode($pelis);
    }
    function PUTAction($params){
        /*print_r($params);
        die();*/
        //echo json_encode($params["poster"]);
        if ($this->peliculasModel->updatePelicula($params)){
            $response = array(
                'codigo' => 201,
                'valor' => 'Se actualizó con exito'
            );
            echo json_encode($response);
        } else {
            $response = array(
                'codigo' => 204,
                'valor' => 'Hubo un error al actualizar'
            );
            echo json_encode($response);
        }
        //echo json_encode($this->peliculasModel->updatePelicula($params));
    }

    function DELETEAction($id){

        if ($this->peliculasModel->deletePelicula($id)){
            $response = array(
                'codigo' => 201,
                'valor' => 'Se BORRO con exito'
            );
            echo json_encode($response);
        }else {
            $response = array(
                'codigo' => 204,
                'valor' => 'Hubo un error al borrar'
            );
            echo json_encode($response);
        }


    }
}
