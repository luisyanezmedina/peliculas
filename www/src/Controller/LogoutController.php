<?php
require_once SECURITY_URL.'Security.php';

class LogoutController extends Controller
{
    public function __construct()
    {
        $this->security = new Security();

    }


    function POSTAction($params)
    {
        // TODO: Implement POSTAction() method.
    }

    function GETAction($id)
    {
        // TODO: Implement GETAction() method.
        $this->security->close();
        if($this->security->getStatus() === 1 || empty($this->security->get('email')))
            exit('Acceso denegado');
    }
}
