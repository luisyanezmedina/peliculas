<?php

require_once MODEL_URL.'PeliculasModel.php';
require_once SECURITY_URL.'Security.php';

class DashboardController extends Controller
{
    private $peliculasModel;

    public function __construct()
    {
        $this->peliculasModel = new PeliculasModel();
        $this->security = new Security();
        $this->security->init();
        if($this->security->getStatus() === 1 || empty($this->security->get('email')))
            exit('Acceso denegado');
    }
    public function POSTAction($params)
    {
        // TODO: Implement POSTAction() method.
    }

    public function GETAction($id)
    {
        // TODO: Implement GETAction() method.
        $arrayPelis = $this->peliculasModel->getPeliculas();
        $param = array('peliculas'=>$arrayPelis);
        $this->renderView(__CLASS__, $param);

    }



}
