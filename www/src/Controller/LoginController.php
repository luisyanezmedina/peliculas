<?php
require_once MODEL_URL.'UserModel.php';
require_once SECURITY_URL.'Security.php';

class LoginController extends Controller
{
    private $user;
    private $security;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->security = new Security();
    }

    public function GETAction($id)
    {
        $param = array('peliculas'=>'nada');
        $this->renderView(__CLASS__, $param);
    }


    public function POSTAction($request_params)
    {
        // TODO: Implement POSTAction() method.
        $query = $this->userModel->getUserByEmail($request_params["email"]);

        if (count($query) > 0) {

            if (password_verify($request_params['password'], $query[0]['password'] )) {

                $this->security->init();
                $this->security->add('email', $query[0]['email']);

                $response = array(
                    'codigo' => 200,
                    'valor' => $query
                );
                echo json_encode($response);
            } else {
                $response = array(
                    'codigo' => 204,
                    'valor' => 'Usuario y contrasena no validos'
                );
                echo json_encode($response);
            }


        } else {
            $response = array(
                'codigo' => 204,
                'valor' => 'No se encontro el usuario'
            );
            echo json_encode($response);
        }

        /*var_dump($request_params);*/
    }

}
