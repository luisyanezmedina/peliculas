<?php
require_once MODEL_URL.'PeliculasModel.php';

class HomeController extends Controller {

    private $peliculasModel;
    public function __construct() {
        $this->peliculasModel = new PeliculasModel();
    }

    function POSTAction($request_params)
    {
        $variables = array('nombre'=>'pepe');
        $this->renderView(__CLASS__, $variables);
    }

    public function GETAction($id = null)
    {
        // TODO: Implement GETTAction() method.
        $peliculas = new PeliculasModel();
        $arrayPelis = $peliculas->getPeliculas();

        $param = array('peliculas'=>$arrayPelis);
        $this->renderView(__CLASS__, $param);
    }
}
