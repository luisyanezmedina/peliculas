<html>
<head>
    <title><?php __CLASS__ ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="assets/js/main.js"></script>
    <link rel="stylesheet" href="assets/css/main.css">

</head>
<body class="bg-light">
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <a class="navbar-brand mr-auto mr-lg-0" href="#"><img src="assets/img/logo1.png" alt=""></a>
</nav>

<div class="nav-scroller bg-white shadow-sm">
    <nav class="nav nav-underline">
        <a class="nav-link active" href="#">Dashboard</a>
        <a class="nav-link" href="#">
            Friends
            <span class="badge badge-pill bg-light align-text-bottom">27</span>
        </a>
        <a class="nav-link" href="#">Explore</a>
        <a class="nav-link" href="#">Suggestions</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
    </nav>
</div>


<main role="main" class="container" style="margin-top: 60px;">
    <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-morado rounded shadow-sm">
        <img class="mr-3" src="assets/img/logo1.png" alt="" width="90" height="48">
        <div class="lh-100">
            <h6 class="mb-0 text-white lh-100">CRUD</h6>
            <small>Peliculas</small>
        </div>
        <div class="float-right col-sm-10" style="text-align: right">
            <a href="Logout">Cerrar Sesión</a>
        </div>
    </div>

    <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="border-bottom border-gray pb-2 mb-0 col-sm-4">Peliculas</h6>
        <div class="col-sm-12 container" style="margin-top: 10px; margin-bottom: 10px;">
            <button class="btn btn-dark" onclick="createPeli();" type="submit">Crear Peli <i class="fa fa-plus" aria-hidden="true"></i> </button>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Poster</th>
                <th scope="col">Genero</th>
                <th scope="col">Cal</th>
                <th scope="col">Accion</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($peliculas as $pelicula)
            {
                ?>
                <tr>
                    <th scope="row"><?php echo $pelicula['id']; ?></th>
                    <td><?php echo substr($pelicula['nombre'],0, 15); ?></td>
                    <td><?php echo substr($pelicula['descripcion'],0, 20); ?></td>
                    <td><?php echo substr($pelicula['poster'],0, 20); ?></td>
                    <td><?php echo $pelicula['genero']; ?></td>
                    <td><?php echo $pelicula['calificacion']; ?> / 10</td>
                    <td>
                        <button onclick="updatePeli(<?php echo $pelicula['id']; ?>);" type="button" class="btn btn-warning btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        <button onclick="readPeli(<?php echo $pelicula['id']; ?>);" type="button" class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button>
                        <button onclick="deletePeli(<?php echo $pelicula['id']; ?>);" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>

    </div>


</main>

<!-- Pop Up -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <form id="pelisForm">
                    <div class="form-row">
                        <div class="col-sm-12" style="margin-top: 5px;" >
                            <label for="staticEmail" class="col-mx-8 col-form-label">#Id de la DB</label>
                            <input id="formPeliId" name="idPeli" class="form-control col-md-4" type="text" readonly value="">
                        </div>
                        <div class="col-sm-12" style="margin-top: 5px;">
                            <input type="text" name="nombre" id="formNombre" class="form-control " placeholder="Nombre">
                        </div>
                        <div class="col-sm-12" style="margin-top: 5px;">
                            <textarea name="descripcion" class="form-control" id="formDescrip" rows="3" placeholder="Descripcion"></textarea>
                        </div>
                        <div class="col-sm-12" style="margin-top: 5px;">
                            <input type="text" name="poster" id="formPoster" class="form-control" placeholder="Url POrtada">
                        </div>
                        <div class="col" style="margin-top: 5px;">
                            <input type="text" name="genero" id="formGenero" class="form-control" placeholder="Genero">
                        </div>
                        <div class="col" style="margin-top: 5px;">
                            <input type="number" id="formCal" name="calificacion" class="form-control"
                                   min="0" max="10" placeholder="Calificacion 0-10">
                        </div>
                        <div class="col-sm-12" style="margin-top: 5px;">
                            <button class="btn btn-primary" id="btnPelisForm" type="submit">Submit form</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- / Pop Up -->

</body>
</html>
