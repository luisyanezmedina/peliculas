<html>
<head>
    <title><?php __CLASS__ ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="assets/js/main.js"></script>
    <link rel="stylesheet" href="assets/css/main.css">

</head>

<body>

<main class="container">
    <div class="container head_menu">
        <nav class="navbar navbar-expand-sm navbar-dark ">
            <a class="navbar-brand" href="#"><img src="assets/img/logo1.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExample03">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="Home" id="Home">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Login">Log In</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="Dashboard">Dashboard</a>
                    </li>

                </ul>

            </div>
        </nav>
    </div>
    <!-- /Menu -->

</main>
<div class="sliderbg movie-items">
    <div class="container">
        <div class="row">
            <?php

            foreach ($peliculas as $pelicula) {
                ?>
                <div class="card movie_item col-sm-2 col-md-3">

                    <img src="<?php echo $pelicula['poster']; ?>" alt="">

                    <div class="container movie_item_txt">
                        <span class="badge badge-warning"><?php echo $pelicula['genero']; ?></span>
                        <a href=""><?php echo $pelicula['nombre']; ?></a>
                        <div class="movie_item_cal">
                            <p><?php echo $pelicula['calificacion']; ?> / 10</p>
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>

                    </div>
                </div>
                <?php
            }
            ?>
            ?>
        </div>
    </div>
</div>


</body>
</html>


