<?php


class UserModel extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getUserByEmail($email) {
        $emailClean = $this->db->real_escape_string($email);
        $sql = "SELECT * FROM usuario WHERE email='{$emailClean}'";
        $query = $this->db->query($sql)->fetch_all(MYSQLI_ASSOC);
        return $query;
    }
}
