<?php

class PeliculasModel extends Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getPeliculas() {

        $query = $this->db->query("SELECT * FROM `pelicula`")->fetch_all(MYSQLI_ASSOC);
        return $query;
    }

    public function getPeliculaById($id) {
        $query = $this->db->query("SELECT * FROM `pelicula` WHERE id = '$id'")->fetch_all(MYSQLI_ASSOC);
        return $query;
    }

    public function updatePelicula($params) {
        $descripcon = $this->db->real_escape_string($params['descripcion']);
        $sql = "UPDATE `pelicula` SET `nombre`='{$params['nombre']}', ";
        $sql .= " `poster`='{$params['poster']}', `genero`='{$params['genero']}',`calificacion`='{$params['calificacion']}', `descripcion`='{$descripcon}'  ";
        $sql .= "WHERE id = '{$params['idPeli']}'";
        $query = $this->db->query($sql);
        return $query;
    }

    public function deletePelicula($id) {
        $query = $this->db->query("DELETE FROM `pelicula` WHERE id = '{$id}'");
        return $query;
    }

    public function createPelicula($params){

        $descripcon = $this->db->real_escape_string($params['descripcion']);
        $sql = "INSERT INTO `pelicula` (`id`, `nombre`, `descripcion`, `poster`, `genero`, `calificacion`, `datos`) ";
        $sql .= "VALUES (NULL, '{$params["nombre"]}', '$descripcon', '{$params["poster"]}', '{$params["genero"]}', '{$params["calificacion"]}', '' )";
        $query = $this->db->query($sql);
        return $query;
    }


}
