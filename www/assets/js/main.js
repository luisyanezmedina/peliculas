$(document).ready(function(){
 // Login form
    $( "#loginForm" ).submit(function(event) {
        event.preventDefault();
        $('.loginOver').fadeIn(1000);
        $.post("Login",$("#loginForm").serialize(),function(res){
            if (res.codigo == 204) {
                $('.loginOver').fadeOut(1000);
                $("#alertError").html(res.valor);
                $("#alertError").fadeIn(1000).delay(5000).fadeOut(1000);
            }else if ( res.codigo == 200) {
                $('.loginOver').fadeOut(1000);
                location.href = 'Dashboard';
            }
            console.log(res.codigo);
        },"json");
    });

    // Funcion para formulario de peliculas
    $( "#pelisForm" ).submit(function(event) {
        event.preventDefault();
        var accion = $( "#btnPelisForm" ).html();
        switch(accion) {
            case 'Actualizar':
                putMethod();
                break;
            case 'Crear':
                // code block
                postPeli();
                break;
            default:
            alert('Error');
        }

    });

}); // fin document ready


/*
Editar pelicula trae los detalles para mostrarlos
 */
function updatePeli(id) {
    $('.bd-example-modal-lg').modal('show')
    this.resetformPelis();
    $( "#btnPelisForm" ).html('Actualizar');
    $.get( "Peliculas/"+id, function( data ) {
        $("#formPeliId").val(data[0].id);
        $("#formNombre").val(data[0].nombre);
        $("#formDescrip").val(data[0].descripcion);
        $("#formPoster").val(data[0].poster);
        $("#formGenero").val(data[0].genero);
        $("#formCal").val(data[0].calificacion);

        //console.log(data);
    },"json");
}

/*
    Mostrar detalles de pelicula
 */
function readPeli(id) {
    $('.bd-example-modal-lg').modal('show');
    this.resetformPelis();
    $( "#btnPelisForm" ).html('Sin funcion').prop("disabled",true);
    $.get( "Peliculas/"+id, function( data ) {
        $("#formPeliId").val(data[0].id);
        $("#formNombre").val(data[0].nombre);
        $("#formDescrip").val(data[0].descripcion);
        $("#formPoster").val(data[0].poster);
        $("#formGenero").val(data[0].genero);
        $("#formCal").val(data[0].calificacion);

        //console.log(data);
    },"json");
}

/*
    Crear peli
 */
function createPeli() {
    this.resetformPelis();
    $('.bd-example-modal-lg').modal('show');

    $( "#btnPelisForm" ).html('Crear');
}
/*
    Eliminar pelicula
 */
function deletePeli(id) {

    $.ajax({
        url: "Peliculas/" + id,
        type: "DELETE",
        data: {'id':id},
        dataType: "json",
        complete: function (response) {
            var respuesta = jQuery.parseJSON(response.responseText);
            if (respuesta.codigo == 201) {
                alert('se borro el registro');
                location.href = 'Dashboard';
            }else {
                alert('algo ocurrio');
            }


        }
    });
}



/*
Reset formulario
 */
function resetformPelis() {
    $("#formPeliId").val('');
    $("#formNombre").val('');
    $("#formDescrip").val('');
    $("#formPoster").val('');
    $("#formGenero").val('');
    $("#formCal").val('');
    $( "#btnPelisForm" ).html('Sin funcion').prop("disabled",false);
}

/*
    PUT Method
 */
function putMethod() {

    $.ajax({
        url: "Peliculas",
        type: "PUT",
        data: $("#pelisForm").serialize(),
        dataType: "json",
        complete: function (response) {
            var respuesta = jQuery.parseJSON(response.responseText);
            if (respuesta.codigo == 201) {
                alert('Se actualizo');
                $('.bd-example-modal-lg').modal('hide')
                location.href = 'Dashboard';
            }else if (respuesta.codigo == 204) {
                alert('Algo ocurrio');
            }

        }
    });
}

/*
POST Method
 */
function postPeli() {
    $.ajax({
        url: "Peliculas",
        type: "POST",
        data: $("#pelisForm").serialize(),
        dataType: "json",
        complete: function (response) {
            //console.log(respuesta.responseText);
            var respuesta = jQuery.parseJSON(response.responseText);
            if (respuesta.codigo == 201) {
                alert('Se actualizo');
                $('.bd-example-modal-lg').modal('hide')
                location.href = 'Dashboard';
            }else if (respuesta.codigo == 204) {
                alert('Algo ocurrio');
            }
        }
    });
}
