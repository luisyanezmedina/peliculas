-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: db
-- Tiempo de generación: 03-02-2021 a las 04:54:00
-- Versión del servidor: 5.6.51
-- Versión de PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `peliculas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(900) NOT NULL,
  `datos` varchar(500) NOT NULL,
  `poster` varchar(300) NOT NULL,
  `genero` varchar(50) NOT NULL,
  `calificacion` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`id`, `nombre`, `descripcion`, `datos`, `poster`, `genero`, `calificacion`) VALUES
(1, 'La Era de hielo', 'Tony Stark creates the Ultron Program to protect the world, but when the peacekeeping program becomes hostile, The Avengers go into action to try and defeat a virtually impossible enemy together. Earth\'s mightiest heroes must come together once again to protect the world from global extinction.', '', 'http://demo.amytheme.com/movie/demo/single-cinema/wp-content/uploads/2019/04/img_9.jpg', 'Animada', 5),
(2, 'Mad Max', 'Tony Stark creates the Ultron Program to protect the world, but when the peacekeeping program becomes hostile, The Avengers go into action to try and defeat a virtually impossible enemy together. Earth\'s mightiest heroes must come together once again to protect the world from global extinction.', 'The Big Bang', 'http://demo.amytheme.com/movie/demo/single-cinema/wp-content/uploads/2019/04/img_6.jpg', 'Accion', 6),
(8, 'El Grinch', 'Dentro de un copo de nieve se encuentra el pueblo de Villaquién, cuyos habitantes, los Quién, hacen frenéticos preparativos para las fiestas navideñas, que celebran con felicidad y alegría, excepto El Grinch, un ser verde, peludo y amargado con un corazón dos tallas más pequeño de lo normal, que detesta la Navidad y a los Quién y vive solo en una cueva en lo alto de una montaña al norte de Villaquién junto con su perro Max. Todos los Quién le temen y no quieren saber nada de él. La pequeña Cindy Lou (Taylor Momsen) cree que todo el mundo está olvidando el verdadero sentido de la Navidad al preocuparse demasiado por los regalos, decoraciones y celebraciones y poco por las relaciones personales. Tras encontrarse con El Grinch en la oficina de correos, se interesa por su historia y descubre que tiene un pasado trágico luego que El Grinch la salvase.', '', 'https://boostifythemes.com/demo/wp/buster/wp-content/uploads/2018/11/rWQVj6Z8kPdsbt7XPjVBCltxq90-768x1152.jpg', 'Aventura', 6),
(11, 'Toy Story', 'Toy Story comienza con una misión de reconocimiento realizada por un grupo de juguetes, encabezados por el vaquero Woody, para identificar los obsequios recibidos por su propietario, Andy, con motivo de su séptimo cumpleaños. Entre los regalos se encuentra una figura de acción, el guerrero espacial Buzz Lightyear, que rápidamente pasa a ser el predilecto del niño. Si bien la mayoría de los juguetes, entre ellos Bo Peep, Mr. Potato Head, Hamm, Slinky y Rex, reciben con entusiasmo a Buzz, Woody busca deshacerse en secreto de él por haberlo sustituido como el muñeco favorito de Andy. Por otra parte, Buzz desconoce su identidad como un juguete y cree que su objetivo es regresar de vuelta a su planeta natal.', '', 'https://boostifythemes.com/demo/wp/buster/wp-content/uploads/2018/04/amY0NH1ksd9xJ9boXAMmods6U0D-300x450-1-280x395.jpg', 'Infantil', 8),
(12, 'Rescatando al Soldado', 'En la mañana del 6 de junio de 1944, comienzo de la invasión de Normandía, los soldados estadounidenses se preparan para desembarcar en la playa de Omaha. Nada más abrirse las puertas de sus lanchas de desembarco son recibidos por un feroz fuego de Ametralladoras MG 42 y artillería alemana, que masacra a muchos de los soldados en cuanto ponen pie en tierra. El capitán John H. Miller, al mando de la compañía Charlie del 2.º Batallón Ranger, sobrevive a la carnicería del desembarco, reúne a un grupo de soldados para intentar penetrar las defensas alemanas y abre brecha para avanzar desde la playa.', '', 'https://boostifythemes.com/demo/wp/buster/wp-content/uploads/2018/04/poster10-683x1024-1-280x395.jpg', 'Guerra', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `email`, `password`) VALUES
(1, 'prueba@prueba.com', '$2y$10$g1tGQRN/IO8nPin.WVhGR.6/dH2TlzPETrGLtcrgZD7bG1uEXm8Y2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
